import cv2 as cv
import numpy as np

'''
frame-кадр, на котором всё рисуется
pos-0 или 1: позиция вывода текста
text- текст для вывода в рамке
'''
def DrawTextInRect(frame, pos,text):
    r_pos=(10, 2)
    r_size=(100,20)
    if pos==1:
        r_pos=(10, 32)
        r_size=(130,50)
    #рамка вокруг текста 
    cv.rectangle(frame, r_pos, r_size, (255,255,255), -1)
    #текст 
    cv.putText(frame, text, (r_pos[0],r_pos[1]+12),
               cv.FONT_HERSHEY_SIMPLEX, 0.5 , (0,0,0))
 

def DrawCustom():
    DrawTextInRect(frame, 0, str(cap.get(cv.CAP_PROP_POS_FRAMES)))
    DrawTextInRect(frame, 1, str(frame.shape))
    DrawTextInRect(mask, 1, str(pixels))
    DrawTextInRect(mask, 0, str(count))
    #рамка вокруг области подсчета
    cv.rectangle(mask,calc_zone[0],calc_zone[1],(255,255,255),1)

cap = cv.VideoCapture('cutted.avi')

#источник https://docs.opencv.org/4.2.0/df/d9d/tutorial_py_colorspaces.html
#опорный кадр цвета
ref_size=50
ref_x=160
ref_y=10
#зона подсчета пикселей
calc_zone=((0,225),(220,255))

count = 0#количество объектов
inFrame = False#наличие объекта
level = 10#порог наличия объекта

while(1):
    # Take each frame
    _, frame = cap.read()
    # Convert BGR to HSV
    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    # define range of blue color in HSV
    bgr_ref=np.median(frame[ref_x:ref_x+ref_size,ref_y:ref_y+ref_size],axis=[0,1])
  
    bgr_ref=np.uint8([[bgr_ref]])
    hsv_ref = cv.cvtColor(bgr_ref,cv.COLOR_BGR2HSV)
    hsv_ref = hsv_ref[0,0]
    #вот так по-идее правильно
    #lower_bound = np.transpose(cv.subtract(hsv_ref, np.uint8([10,100,100])))[0]
    #upper_bound = np.transpose(cv.add(hsv_ref, np.uint8([10,255,255])))[0]
    #но вот так работает
    lower_bound = np.uint8([hsv_ref[0]-10,50,50]) 
    upper_bound = np.uint8([hsv_ref[0]+10,255,255]) 
    # Threshold the HSV image to get only reference colors
    mask = cv.inRange(hsv, lower_bound, upper_bound)

    sample = mask[calc_zone[0][1]:calc_zone[1][1], calc_zone[0][0]:calc_zone[1][0]]
    pixels = sum(sum(sample))//255
 
    if (pixels > level):
        inFrame = True
    else:
        if inFrame:
            count += 1
        inFrame = False

    # Bitwise-AND mask and original image
    res = cv.bitwise_and(frame,frame, mask= mask)
    DrawCustom()
   
    cv.imshow('ref',frame[ref_x:ref_x+ref_size,ref_y:ref_y+ref_size])
    cv.imshow('frame',frame)
    cv.imshow('mask',mask)
    cv.imshow('res',res)

    if cv.waitKey(20) == ord('q'):
        break
cv.destroyAllWindows()